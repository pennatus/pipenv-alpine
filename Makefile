
.PHONY: all clean build docs lint deploy test docker-push

###
# stages:
#   - clean: destroy docker image and containers
#   - build: build image
#   - deploy: vagrant push
#
# How to use:
#  - Add a specific job as a target in the right stage
#  - Add job target to the stage target
#  - append $(DEPLOY_ENV) to the stage target to instance the right job target, if applicable
###

###
# CI/CD control variables
###
DEPLOY_ENV ?= DEV

# this can be app/service/nickname serves as a generic name that can be used anywhere
SERVICE_NAME ?= pipenv-alpine

# Docker specific variables
DOCKER_REGISTRY ?= registry.gitlab.com
DOCKER_TAG_VERSION ?= $(shell git symbolic-ref --short HEAD)
DOCKER_IMAGE_PATH ?= pennatus/$(SERVICE_NAME)
DOCKER_IMAGE ?= $(DOCKER_REGISTRY)/$(DOCKER_IMAGE_PATH):$(DOCKER_TAG_VERSION)

###
# private variables
###

# Gives us a target we can use to trigger a rebuild of the Dockerfile
BUILD_DOCKER = .build-docker

###
# pipeline
###
all: clean build deploy

###
# STAGE: clean
###
clean: clean-docker

clean-docker:
	docker container rm -v -f $(SERVICE_NAME)
	docker image rm -f $(DOCKER_IMAGE)
	rm $(BUILD_DOCKER)

###
# STAGE: build
###
build: $(BUILD_DOCKER) 

$(BUILD_DOCKER): Dockerfile
	docker build --rm -t $(DOCKER_IMAGE) .
	touch $(BUILD_DOCKER)

###
# STAGE: deploy jobs
###
deploy: docker-push

docker-push:
	docker tag $(DOCKER_IMAGE) $(DOCKER_REGISTRY)/$(DOCKER_IMAGE_PATH):latest
	docker push $(DOCKER_IMAGE)
	docker push $(DOCKER_REGISTRY)/$(DOCKER_IMAGE_PATH):latest

