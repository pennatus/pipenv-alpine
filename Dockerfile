FROM python:3.7-alpine

ENV PYTHONUNBUFFERED 1
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

# Credit for this section goes to: https://medium.com/c0d1um/building-django-docker-image-with-alpine-32de65d2706
# Added git&make so you can integrate easily with gitlab CI/CD using private 
# repositorys to pip install from.
RUN mkdir /app

WORKDIR /app

ONBUILD ARG EXTRA_PIPENV_OPTIONS
# Use this to install additional pip packages not in Pipfile (private gitlab repositories
ONBUILD ARG EXTRA_PIP_PACKAGES

# -- Adding Pipfiles
ONBUILD COPY Pipfile Pipfile
ONBUILD COPY Pipfile.lock Pipfile.lock

# FIXME: put this back in the !!!!
# -- Install dependencies:
ONBUILD RUN apk add --no-cache --virtual .build-deps make \
                ca-certificates gcc linux-headers musl-dev git \
                libffi-dev libressl-dev jpeg-dev zlib-dev bzip2-dev \
            && pip3 install pipenv \
            &&  set -ex \     
            && pipenv --clear install --system ${EXTRA_PIPENV_OPTIONS} --deploy \
            && if ! [ -z "${EXTRA_PIP_PACKAGES}" ]; then  pip --no-cache install ${EXTRA_PIP_PACKAGES}; fi \
            && find /usr/local \
               \( -type d -a -name test -o -name tests \) \
               -o \( -type f -a -name '*.pyc' -o -name '*.pyo' \) \
               -exec rm -rf '{}' + \
            && runDeps="$( \
                scanelf --needed --nobanner --recursive /usr/local \
                | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
                | sort -u \
                | xargs -r apk info --installed \
                | sort -u \
                )" \
           && apk add --virtual .rundeps $runDeps \
           && apk del .build-deps

# Credit for this section goes to: https://github.com/pypa/pipenv/blob/master/Dockerfile
# --------------------
# - Using This File: -
# --------------------

# FROM registry.gitlab.com/pennatus/pipenv-alpine

# COPY . /app

# -- Replace with the correct path to your app's main executable
# CMD python3 main.py
